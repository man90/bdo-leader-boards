# BDO Leaderboards
This repository used to contain a web-based application that allows you to see and compare various achievements of guild members in Black Desert Online. Now it's just a stub with a single page that redirects you to the project's new location.

## You can access the application on [hemlo.cc](https://bdo.hemlo.cc/leaderboards/).
## The source code is now stored on [GitHub](https://github.com/man90es/BDO-Leaderboards).
